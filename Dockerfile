FROM gradle:jdk11 as assemble
WORKDIR /assemble
COPY . /assemble
RUN gradle assemble --no-daemon

FROM arm32v7/adoptopenjdk:16
WORKDIR /build
COPY --from=assemble /assemble/build/libs/stock-api-0.0.1-SNAPSHOT.jar stock-api.jar
EXPOSE 8080
CMD java -jar stock-api.jar