package org.stocks.stockapi.repository;


import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.stocks.stockapi.exception.StockPricesFileNotFoundException;
import org.stocks.stockapi.model.StockData;
import org.stocks.stockapi.model.StockPrices;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

@Slf4j
public class StockPricesRepository {

    private static final String STOCKS_LOCATION = "/store";

    public List<StockData> getAllPrices() {
        File folder = new File(STOCKS_LOCATION);
        List<StockData> stocks = new ArrayList<>();
        for (File file : Objects.requireNonNull(folder.listFiles())) {
            String sym = file.getName().split("_")[0];
            StockData stockData = new StockData();
            stockData.setSymbol(sym);
            try {
                List<StockPrices> beans = new CsvToBeanBuilder<StockPrices>(new FileReader(file.getAbsolutePath()))
                        .withType(StockPrices.class)
                        .build()
                        .parse();
                stockData.setStockPrices(beans);
            } catch (FileNotFoundException ignore) {

            }

            stocks.add(stockData);
        }

        return stocks;
    }

    public StockData getDataBySymbol(String symbol) {
        String location = STOCKS_LOCATION + "/" + symbol.toUpperCase(Locale.ROOT) + "_prices.csv";
        StockData stockData = new StockData();
        stockData.setSymbol(symbol);
        try {
            log.debug("Trying to access stock prices file: " + location);
            List<StockPrices> beans = new CsvToBeanBuilder<StockPrices>(new FileReader(location))
                    .withType(StockPrices.class)
                    .build()
                    .parse();
            stockData.setStockPrices(beans);
        } catch (FileNotFoundException ignore) {
            log.error("Stock prices file not found: " + location);
            throw new StockPricesFileNotFoundException();
        }

        log.debug("Found stock prices for: " + symbol);
        return stockData;
    }
}
