package org.stocks.stockapi.model;

import lombok.*;

import java.util.Date;

@Data
public class StockPrices {
    private String date;
    private Float close;
}
