package org.stocks.stockapi.model;

import lombok.*;

import java.util.List;
import java.util.Map;

@Data
public class StockData {
    private String symbol;
    private List<StockPrices> stockPrices;
}
