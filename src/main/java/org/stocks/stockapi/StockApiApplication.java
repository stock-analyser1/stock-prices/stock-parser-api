package org.stocks.stockapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.stocks.stockapi.repository.StockPricesRepository;

@SpringBootApplication
public class StockApiApplication {

    @Bean
    public StockPricesRepository stockPricesRepository() {
        return new StockPricesRepository();
    }

    public static void main(String[] args) {
        SpringApplication.run(StockApiApplication.class, args);
    }

}
