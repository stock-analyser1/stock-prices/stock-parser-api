package org.stocks.stockapi.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.stocks.stockapi.model.StockData;
import org.stocks.stockapi.repository.StockPricesRepository;

import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("stocks/prices")
@Slf4j
public class StockPricesController {

    @Autowired
    StockPricesRepository stockPricesRepository;

    @GetMapping
    public List<StockData> getStockPrices() {
        return stockPricesRepository.getAllPrices();
    }

    @GetMapping("{symbol}")
    public StockData getStockPrice(@PathVariable String symbol) {
        log.debug("GET /stocks/prices/" + symbol);
        log.info("Request for stock with symbol: " + symbol);
        return stockPricesRepository.getDataBySymbol(symbol.toLowerCase(Locale.ROOT));
    }
}
