package org.stocks.stockapi.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileNotFoundException;

@SpringBootTest
class StockPricesRepositoryTest {

    @Autowired
    private StockPricesRepository stockPricesRepository;

    @Test
    void getStockPricesBySymbol() throws FileNotFoundException {
        System.out.println(stockPricesRepository.getAllPrices());
    }
}